package retriever

import (
	"fmt"
	"testing"
)

func TestRetrieveCurrency(t *testing.T) {
	number, err := RetrieveCurrency("EUR", "NOK")

	if err != nil {
		fmt.Println(err)
	}

	if number <= 0 {
		t.Fatal("Number retrieved is not valid")
	}
}
