FROM golang

ADD . /go/src/bitbucket.org/Klyngen/cloudass2

RUN go install bitbucket.org/Klyngen/cloudass2/cmd/webhook/

ENTRYPOINT /go/bin/webhook

EXPOSE 8080
