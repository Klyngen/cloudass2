package database

import (
	"errors"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

// OpenConnection - Creates a connection with the databe
func OpenConnection() *mgo.Database {
	conn, err := mgo.Dial("mongodb://admin:1@ds243805.mlab.com:43805/currencies")
	//conn, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic("Could not establish connection")
	}

	database := conn.DB("currencies")

	return database
}

// AddWebhook - adds a new webhook to the database
func AddWebhook(data RegisterPayload, database *mgo.Database) (id string, err error) {
	data.ID = bson.NewObjectId()
	err = database.C("webhooks").Insert(&data)

	if err != nil {
		return
	}

	id = data.ID.Hex()
	return
}

// RemoveWebHook - deletes webhook by using the webhook id
func RemoveWebHook(value string, database *mgo.Database) error {

	if len(value) == 24 {
		ID := bson.ObjectIdHex(value)
		err := database.C("webhooks").Remove(bson.M{"_id": ID})

		if err != nil {
			fmt.Println(err)
			return errors.New("Could not remove the webhook with the given ID")
		}
	} else {
		return errors.New("Illegal length of string")
	}

	return nil
}

// InsertCurrencyData - Inserts currency-data into the database with timestamp
func InsertCurrencyData(name string, value float64, database *mgo.Database) error {
	currentTime := time.Now().Format("2006-01-02") // Retrieves the current time

	err := database.C("currency").Insert(bson.M{"currency": name, "rate": value, "time": currentTime})

	if err != nil {
		return errors.New("Could not insert currency")
	}

	return nil
}

// GetAverageCurrency - returns the average from the last seven days
func GetAverageCurrency(database *mgo.Database, field string) float64 {
	fmt.Println(field)
	var result []CurrencyContainer

	pastTime := time.Now().AddDate(0, 0, -7).Format("2006-01-02") // Get date seven days ago
	err := database.C("currency").Find(bson.M{"currency": field, "time": bson.M{"$gt": pastTime}}).All(&result)

	fmt.Println(len(result))
	fmt.Println(result)

	if err != nil {
		panic(err)
	}

	var sum float64

	for _, tall := range result {
		sum += tall.Rate

	}
	return (sum / (float64(len(result))))
}

// GetHookInformation - retrieves information from database
func GetHookInformation(database *mgo.Database, id string) (payload RegisterPayload, err error) {
	if len(id) != 24 {
		err = errors.New("Invalid ID-string. has to have 24 characters")
	} else {
		ID := bson.ObjectIdHex(id)
		err = database.C("webhooks").Find(bson.M{"_id": ID}).One(&payload)
		fmt.Println(payload)
		if len(payload.BaseCurrency) == 0 && len(payload.TargetCurrency) == 0 && len(payload.WebhookURL) == 0 {
			err = errors.New("Empty result")
		}
	}

	return
}

// GetAllWebHooks - retrieves all webhooks for processing and invoking
func GetAllWebHooks(database *mgo.Database) (payloads []RegisterPayload, err error) {
	err = database.C("webhooks").Find(nil).All(&payloads)
	return
}
