package database

import (
	"fmt"
	"testing"
)

func TestInsertHook(t *testing.T) {
	connection := OpenConnection()

	var payload RegisterPayload
	payload.BaseCurrency = "NOK"
	payload.TargetCurrency = "EUR"
	payload.WebhookURL = "TestCaseURL"
	payload.MaxTriggerValue = 0.1
	payload.MinTriggerValue = 0.01

	result, err := AddWebhook(payload, connection)

	if err != nil {
		t.Fatal("Could not add stuct data to database")
	}

	if len(result) != 0 {
		information, err := GetHookInformation(OpenConnection(), result)

		if err != nil {
			t.Fatal("Could not retrieve information from the database")
		}
		if information.WebhookURL != payload.WebhookURL &&
			information.MinTriggerValue != payload.MinTriggerValue &&
			information.MaxTriggerValue != payload.MaxTriggerValue &&
			information.BaseCurrency != payload.BaseCurrency &&
			information.TargetCurrency != payload.TargetCurrency {
			t.Fatal("Data retrieved not the same as data sendt")
		}

	}

	if len(result) == 0 {
		t.Fatal("No id returned from insertion")
	} else {
		removeError := RemoveWebHook(result, connection)
		fmt.Println(result)
		if removeError != nil {
			fmt.Println(removeError)
			t.Fatal("Could not remove the webhook")
		}
	}

}

func TestInsertCurrency(t *testing.T) {
	connection := OpenConnection()

	if InsertCurrencyData("NOK", 5.5, connection) != nil {
		t.Fatal("Could not insert currency")
	}
}
