package main

import (
	"bitbucket.org/Klyngen/cloudass2/controller"
)

func main() {
	controller.StartWebServer()
}
