package main

import (
	"bitbucket.org/Klyngen/cloudass2/controller"
	"fmt"
	"github.com/robfig/cron"
	"time"
)

func main() {
	controller.CheckHooks()
	timer := cron.New()
	error := timer.AddFunc("@every 8h", func() { controller.CheckHooks() })
	timer.Start()
	fmt.Println(timer.Entries())

	if error != nil {
		panic("Webserver not started. Wont try again, im lazy")
	} else { // Thread so the cron subprocess runs
		for true {
			time.Sleep(30 * time.Hour)
		}
	}

}
