package database

import (
	"gopkg.in/mgo.v2/bson"
)

// RegisterPayload containing values for creating a new webhook
type RegisterPayload struct {
	ID              bson.ObjectId `bson:"_id" json:"_id"`
	WebhookURL      string        // Url of the given webhook
	BaseCurrency    string
	TargetCurrency  string
	MinTriggerValue float64 // Below this minimum the service will invoke
	MaxTriggerValue float64 // Above this maximum the service will invoke
	Discord         bool
}

// InvokePayload - contains the payload for invoking a webhook
type InvokePayload struct {
	BaseCurrency    string
	TargetCurrency  string
	CurrentRate     float64
	MinTriggerValue float64
	MaxTriggerValue float64
}

// DeletePayload - information about deletion is unmarshaled into this
type DeletePayload struct {
	ID string
}

// CurrencyContainer - will contain currency when unmarshaled
type CurrencyContainer struct {
	Rate     float64
	Currency string
}

// Response - Structure of the response given to a user when a json response is in order
type Response struct {
	Header      string
	Description string
}

// Identification - is responible for carrying id from GET-request
type Identification struct {
	ID string
}

// DiscordEmbed - to be placed in array in other structs
type DiscordEmbed struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

// DiscordInvoke - The information sendt to the server
type DiscordInvoke struct {
	Embeds   []DiscordEmbed `json:"embeds"`
	Username string         `json:"username"`
}
