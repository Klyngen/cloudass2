package controller

// TODO - CREATE DATABASE FUNCTION - DONE
// TODO - ADD NEW WEBHOOK TO DATABASE - DONE
// TODO - REMOVE A WEBHOOK - DONE
// TODO - GET INFORMATION ABOUT EXISTING HOOKS - DONE
// TODO - POST TO A WEBHOOK
// TODO - RETRIEVE CURRENCY INFORMATION - DONE
// TODO - ON CREATION OF WEBHOOK, RETURN an ID - DONE
// TODO - ON RUN, PARSE THE EXISTING HOOKS and MAYBE INVOKE ONE OF THEM - DONE

// TODO - Add cron funtionality
// TODO - Write tests to reach the 40% mark
// TODO - FIX getting information about the webhook - DONE

import (
	"bitbucket.org/Klyngen/cloudass2/database"
	"bitbucket.org/Klyngen/cloudass2/retriever"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// SimpleResponse - returna response consisting of just one line of text
func SimpleResponse(rw http.ResponseWriter, message string) {
	rw.Header().Set("Content-Type", "Text")
	fmt.Fprintf(rw, message)
}

// DetailedResponse - returns a json response with http-protocol code
func DetailedResponse(rw http.ResponseWriter, message string, code string) {
	var response database.Response
	response.Description = message

	response.Header = code

	rw.Header().Set("Content-Type", "application/json")

	output, error := json.Marshal(&response)

	if error != nil {
		fmt.Println(error)
	} else {
		fmt.Fprintf(rw, "%s", output)
	}

}

// JSONResponse - createAgilefant runs on Java, Tomcat, and MySQL. The source code and license can be found on GitHub.s response from a struct
func JSONResponse(rw http.ResponseWriter, data interface{}) {
	rw.Header().Set("Content-Type", "application/json")

	output, error := json.Marshal(&data)

	if error != nil {
		fmt.Println("Could not marshal struct")
		DetailedResponse(rw, "Something went wrong with the server", "500")
	} else {
		fmt.Fprintf(rw, "%s", output)
	}
}

// PostNewWebhook - unms smallarshals data to then add it to the database
func PostNewWebhook(req *http.Request) (id string, err error) {
	var structure database.RegisterPayload

	structure.Discord = false // The default should be false to not fail the task

	err = unmarshalToStruct(req, &structure)

	if err != nil {
		fmt.Println("Could not read data from request")
		return
	}

	id, err = database.AddWebhook(structure, database.OpenConnection())
	return
}

// GetHookInformation - Returns json data from a specific webhook id
func GetHookInformation(req *http.Request) (res database.RegisterPayload, err error) {
	//res, err = database.GetHookInformation(database.OpenConnection(), req.URL.Query().Get("id"))
	//	fmt.Println(req.URL.RequestURI())
	res, err = database.GetHookInformation(database.OpenConnection(), strings.TrimLeft(req.URL.RequestURI(), "hooker/"))
	return
}

// DeleteHook - Tries to interprete the deleteCommand, then delets message
func DeleteHook(req *http.Request) error {

	database.RemoveWebHook(strings.TrimLeft(req.URL.RequestURI(), "hooker/"), database.OpenConnection())
	return nil
}

func unmarshalToStruct(req *http.Request, dest interface{}) error {
	body, err := ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Println(err)
	}

	marshalErr := json.Unmarshal(body, dest)
	return marshalErr
}

// WebHookHandler - Main handler for insertion, deletion and basic information about webhooks
func WebHookHandler(rw http.ResponseWriter, req *http.Request) {
	switch method := req.Method; method {
	case "POST":
		id, err := PostNewWebhook(req)
		if err != nil {
			SimpleResponse(rw, "Could not insert webhook")
		} else {
			SimpleResponse(rw, id)
		}
	case "DELETE":
		if DeleteHook(req) == nil {
			DetailedResponse(rw, "Deleted webhook", "200")
		} else {
			DetailedResponse(rw, "Bad request", "400")
		}
	case "GET":
		structure, err := GetHookInformation(req)
		if err == nil {
			fmt.Println(structure)
			JSONResponse(rw, structure)
		} else {
			DetailedResponse(rw, "No such webhook", "404")
			fmt.Println(err)
		}
	}
}

// latestHandler - gets the latest currency rates directly from the fixme-page
func latestHandler(rw http.ResponseWriter, req *http.Request) {
	// TODO - add code that adds currency data to the database
	var payload database.RegisterPayload

	unmarshalToStruct(req, &payload)

	number, err := retriever.RetrieveCurrency(payload.BaseCurrency, payload.TargetCurrency)

	if err != nil {
		fmt.Println(err)
		SimpleResponse(rw, "400 Could not get currency data")
	} else {
		SimpleResponse(rw, strconv.FormatFloat(number, 'f', 3, 64))
		database.InsertCurrencyData(payload.BaseCurrency+payload.TargetCurrency, number, database.OpenConnection())
	}
}

func averageHandler(rw http.ResponseWriter, req *http.Request) {
	// TODO - add code that adds currency data to the database
	var payload database.RegisterPayload

	unmarshalToStruct(req, &payload)

	if len(payload.BaseCurrency) != 0 && len(payload.TargetCurrency) != 0 {
		num := database.GetAverageCurrency(database.OpenConnection(), payload.TargetCurrency+payload.BaseCurrency)
		SimpleResponse(rw, strconv.FormatFloat(num, 'f', 3, 64))
	} else {
		fmt.Println("Badly formed request")
		SimpleResponse(rw, "400 Could not generate average")
	}
}

// invokeWebHook - invokes a webhook
func invokeWebHook(data interface{}, url string) {
	// Function inspired by https://stackoverflow.com/questions/24455147/how-do-i-send-a-json-string-in-a-post-request-in-go
	output, err := json.Marshal(data)

	if err != nil {
		fmt.Println(err)
	} else {
		request, err := http.NewRequest("POST", url, bytes.NewBuffer(output))
		fmt.Println(bytes.NewBuffer(output))
		if err != nil {
			fmt.Println(err)
		} else {
			request.Header.Set("Content-Type", "application/json")

			client := &http.Client{}

			res, err := client.Do(request)
			fmt.Println(res)
			fmt.Println(err)
		}
	}
}

// CheckHooks - Loops through all webhooks to see if any webhook should be invoked
func CheckHooks() {
	data, err := database.GetAllWebHooks(database.OpenConnection())
	var discPayload database.DiscordInvoke
	var Payload database.InvokePayload
	var Embeds database.DiscordEmbed
	if err != nil {
		fmt.Println(err)
	} else {
		for _, element := range data { // Loops the elements from the database
			fmt.Println(element) // Print for debugging
			// Retrieve currency data
			// Check if it is within min and max
			number, reterr := retriever.RetrieveCurrency(element.BaseCurrency, element.TargetCurrency)

			if reterr != nil {
				fmt.Println("could not get currencydata for webhook. Skipping it")
			} else {

				database.InsertCurrencyData(element.BaseCurrency+element.TargetCurrency, number, database.OpenConnection())

				if number < element.MinTriggerValue || number > element.MaxTriggerValue {
					if element.Discord {
						Embeds.Title = "Currency warning"
						Embeds.Description = element.TargetCurrency + " is now " + strconv.FormatFloat(number, 'f', 5, 64) + "\n"
						Embeds.Description += "MaxTriggerValue = " + strconv.FormatFloat(element.MaxTriggerValue, 'f', 5, 64) + "\n"
						Embeds.Description += "MinTriggerValue = " + strconv.FormatFloat(element.MinTriggerValue, 'f', 5, 64) + "\n"

						discPayload.Embeds = append(discPayload.Embeds, Embeds)
						invokeWebHook(discPayload, element.WebhookURL)
					} else {
						Payload.BaseCurrency = element.BaseCurrency
						Payload.TargetCurrency = element.TargetCurrency
						Payload.CurrentRate = number
						Payload.MinTriggerValue = element.MinTriggerValue
						Payload.MaxTriggerValue = element.MaxTriggerValue
						invokeWebHook(Payload, element.WebhookURL)
					}
				} else {
					fmt.Println("The element was not outside the parameters")
				}
			}
		}
	}
}

// StartWebServer - starts a webserver and adds handlers to various paths
func StartWebServer() {
	PORT := os.Getenv("PORT")
	fmt.Println(PORT)
	http.HandleFunc("/hooker", WebHookHandler)
	http.HandleFunc("/hooker/", WebHookHandler)
	http.HandleFunc("/hooker/latest", latestHandler)
	http.HandleFunc("/hooker/average", averageHandler)
	log.Fatal(http.ListenAndServe(":"+PORT, nil))
}
