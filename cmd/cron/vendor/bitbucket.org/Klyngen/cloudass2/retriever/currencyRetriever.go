package retriever

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// CurrencyResponse - is to hold the data of the retrieced currency data
type CurrencyResponse struct {
	Rates map[string]float64
}

// RetrieveCurrency - gets currencydata from external API
func RetrieveCurrency(base string, target string) (number float64, err error) {
	// Generate URL
	// REQUEST
	var rates CurrencyResponse
	var baseURL = "http://api.fixer.io/latest?base=" + base + "&symbols=" + target

	resp, err := http.Get(baseURL)

	if err != nil {
		fmt.Println(err)
		fmt.Println("Fetch error")
		return
	}

	content, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatal("UnmarshalError")
		log.Fatal(err)
		return
	}
	err = json.Unmarshal(content, &rates)
	number = rates.Rates[target]
	return
}
